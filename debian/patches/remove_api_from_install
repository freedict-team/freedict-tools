Description: strip unused scripts and targets from installation
 By default, the upstream Makefile installs Python scripts in a very
 non-standard way to a very non-standard location. Since these are not required
 for building dictionaries, they are not installed with this package. This also
 removes the Make targets to invoke them.
Author: Sebastian Humenda <shumenda@gmx.de>
Last-Update: 2020-12-31

Index: freedict-tools/Makefile
===================================================================
--- freedict-tools.orig/Makefile
+++ freedict-tools/Makefile
@@ -10,37 +10,13 @@ PREFIX ?= usr
 DESTDIR ?= 
 INSTALLDIR ?= $(abspath $(DESTDIR)/$(PREFIX)/share/freedict)
 
-dirs = api JMdict lib mk xquery xsl/inc
+dirs = JMdict lib mk xquery xsl/inc
 TARGET_INSTALL_DIRS = $(addprefix $(INSTALLDIR)/tools/, $(dirs))
 
 
-api: #! generate the api with information about all dictionaries and their downloads at the configured api path
-api:
-	$(call mount_or_reuse); \
-		$(call exc_pyscript,fd_api) || sleep 1; \
-		$(call umount_or_keep)
-	@$(MAKE) -C $(FREEDICT_TOOLS) --no-print-directory api-validation
-
-# allow retrieval of API path from Makefile and from rule below
-get_api_path=$(call exc_pyscript,fd_file_mgr,-a) | tr -d '\n'
-api-path: #! print the output directory to the generated API file (read from configuration) (trailing newline is removed)
-	@$(call get_api_path)
-
-api-validation: #! validate the freedict-database.xml against its RNG schema
-	xmllint --noout --relaxng freedict-database.rng $(shell $(call get_api_path))/freedict-database.xml
-
-
-
-
 mount: #! mount or synchronize FreeDict releases / generated dictionaries
 	$(call exc_pyscript,fd_file_mgr,-m)
 
-need-update: #! queries for unreleased dictionaries or for those with newer source changes
-	@$(call mount_or_reuse); \
-		$(call exc_pyscript,fd_api,-n)\
-			|| sleep 1; \
-		$(call umount_or_keep)
-
 umount: #! runs umount / clean up actions for unmounting remote volumes (if SSH is used)
 	@$(call exc_pyscript,fd_file_mgr,-u)
 
@@ -146,9 +122,8 @@ mk_venv: #! initialise a new (Python) vi
 	fi
 
 # NOTE: the directories below HAVE to be on one line
-INSTALL_DIRS := api api/generator api/file_manager api/generator/apigen mk xquery xsl/inc
-INSTALL_PATHS := $(wildcard api/*.py api/generator/*.py api/generator/apigen/*.py api/file_manager/*.py \
-	freedict-database.rng \
+INSTALL_DIRS := mk xquery xsl/inc
+INSTALL_PATHS := $(wildcard freedict-database.rng \
 	mk/*.mk xsl/inc/* \
 	xquery/* xsl/tei2c5.xsl)
 install: #! install the tools to $$DESTDIR/$PREFIX/share/freedict (default /usr/local/share/freedict)
@@ -173,4 +148,4 @@ release: $(BUILD_DIR)/freedict-tools-$(V
 release-path: #! print the output directory to which releases are deployed (read from configuration); trailing newline is removed
 	@$(call exc_pyscript,fd_file_mgr,-r) | tr -d '\n'
 
-.PHONY: release install api all mount umount api-validation
+.PHONY: release install all mount umount
